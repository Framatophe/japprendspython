#!/usr/bin/python3.2
# -*-coding:Utf-8 -*

# Programme pour afficher des tables de multiplication

nb = input("Saisissez un nombre : ") # On demande le nombre a l'utilisateur
nb = int(nb) # On fait en sorte que la variable nb soit un entier
i = 0 # c'est la variable que nous allons incrémenter dans la  boucle

while i < 19: # Tant que i sera inférieur à 19
        print(i+1, " fois ", nb, " égal ", (i + 1) * nb) # Tu imprimes ..
        i += 1 # on incrémente i de 1 à chaque tour


input("Appuyez sur ENTREE pour fermer ce programme")